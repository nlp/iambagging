#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Generate random sentences
"""

import random

# generate a list of words
words = ['word'+str(i) for i in range(10)]
# take a random length of document from n_min to n_max
n_min, n_max = 4, 15
len_choice = [random.choice(range(n_min,n_max)) for _ in range(12)]
# take randomly a word in words up to size given in len_choice
test_catalog = [[random.choice(words) for _ in range(lc)] for lc in len_choice]


def random_catalog(size_vocab=10,
                   size_documents=list(),
                   size_catalog=100,
                   weights_vocab=list()):
    if not list(size_documents):
        size_documents = [1,size_vocab]
    if not list(weights_vocab) or len(list(weights_vocab))!=int(size_vocab):
        weights_vocab = None
    tokens = ['w'+str(i) for i in range(int(size_vocab))]
    catalog = [random.choices(tokens,
                              k=random.choice(range(*size_documents)),
                              weights=weights_vocab)
               for _ in range(size_catalog)]
    
    return catalog


size_vocab = 20000
weights_vocab = [5,5,5,5,5,20,2,2,2,2] + [1]*(size_vocab-10)
size_documents = [25,155]
size_catalog = 100
rand_cat = random_catalog(weights_vocab=weights_vocab,
                          size_documents=size_documents,
                          size_catalog=size_catalog,
                          size_vocab=size_vocab)
