#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test of the BagOfWords class

Run as 
```bash
python3 -m unittest -v test/test_NextOfWords.py
```
from the main folder in order to get the module elements prior to its installation.
"""

import unittest as ut

import numpy as np

from iambagging.nextofwords import NextOfWords as NOW
from iambagging.bagofwords import BagOfWords
from scripts.generate_random_catalog import test_catalog as catalog

class TestNextOfWords(ut.TestCase):
    
    def setUp(self):
        self.bow = NOW(verbose=False)
        self.bow.fit_transform(catalog)
        return None

    def test_exist(self):
        """now, vocabularray and slices existence, and their raising errors"""
        self.assertTrue(hasattr(self.bow,'bag'))
        self.assertTrue(hasattr(self.bow,'vocabularray'))
        self.assertTrue(hasattr(self.bow,'doctionarray'))
        self.assertTrue(hasattr(self.bow,'slices'))
        self.assertIsNone(self.bow._check_init())
        return None
    
    def test_reconstruct_catalog(self):
        """Verify that the catalog reconstructed from the NoW is the given one"""
        catalog_test = self.bow.reconstruct_catalog()
        self.assertEqual(len(catalog_test), len(catalog))
        for doc, doc_test in zip(catalog, catalog_test):
            self.assertEqual(doc, doc_test)
        return None

    def test_construct_slices(self):
        """Compare the slices created during the construction of the bag and the construct_slices method"""
        slices = self.bow.construct_slices()
        self.assertEqual(len(slices), len(self.bow.slices))
        self.assertEqual(slices, self.bow.slices)
        return None    

    def test_save(self):
        """save and load methods"""
        self.bow.save('test/data', add_time_slug=False)
        new_bow = NOW(verbose=False).load('test/data')
        self.assertEqual(self.bow.bag.shape, new_bow.bag.shape)
        self.assertEqual(self.bow.shape, new_bow.shape)
        self.assertEqual(len(self.bow.bag.data), len(new_bow.bag.data))
        self.assertEqual(self.bow.slices, new_bow.slices)
        now_diff = self.bow.bag - new_bow.bag
        self.assertEqual(len(now_diff.data), 0)
        return None   

class TestNextOfWordsBow(ut.TestCase):
    
    def setUp(self):
        self.now = NOW(verbose=False)
        self.now.fit_transform(catalog)
        self.bow = BagOfWords(verbose=False)
        self.bow.fit(catalog)
        return None
    
    def test_to_bow(self):
        bow = self.now.bow
        self.assertEqual(set(bow.vocabularray), set(self.now.vocabularray))
        self.assertEqual(bow.shape, self.now.shape)
        count1 = {tok: c for tok, c in zip(self.bow.vocabularray, 
                                           self.bow.count()[0])}
        count2 = {tok: c for tok, c in zip(bow.vocabularray, 
                                           bow.count()[0])}
        self.assertEqual(count1, count2)
        return None

class TestNextOfWordsGraph(ut.TestCase):
    
    def setUp(self):
        self.bow = NOW(verbose=False)
        self.bow.fit_transform(catalog)
        return None
    
    def test_adjacency_list(self):
        """Verify that the adjacency list is the expected one"""
        adjlist = self.bow.construct_adjacency_list(tokens=True)
        # make an obvious adjacency list, from scratch
        adjlist_ = {tok: {} for tok in adjlist.keys()}
        for document in catalog:
            for tok1, tok2 in zip(document[:-1], document[1:]):
                try:
                    adjlist_[tok1][tok2] += 1
                except KeyError:
                    adjlist_[tok1][tok2] = 1
        self.assertEqual(adjlist, adjlist_)
        return None
    
    def test_adjacency_matrix(self):
        """Verify that the adjacency matrix and the adjacency list are compatible"""
        adjmat = self.bow.adjacency_matrix
        vocab = set(tok for doc in catalog for tok in doc)
        self.assertEqual(adjmat.shape, (len(vocab), len(vocab)))
        adjlist = self.bow.adjacency_list
        for id1, v in adjlist.items():
            for id2, count in v.items():
                self.assertEqual(adjmat[id1,id2], count)
        return None        
        
if __name__ == '__main__':
    ut.main()


