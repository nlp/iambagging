#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Base class for the bag-of-words like extractors, in case the bag contains one more columns that records the position of the token inside the document.
"""

from datetime import datetime as dt
from typing import Iterable, Tuple, List
from collections import defaultdict, Counter

import numpy as np
import scipy.sparse as sp
import networkx as nx
from tqdm import tqdm

from .bagbase import BagBase
from .bagofwords import BagOfWords

class DirectedBag(BagBase):
    
    @property
    def shape(self,) -> Tuple[int, int]:
        """Give the shape of the bag in the form DxV."""
        return (len(self.slices), max(0, self.bag.shape[1]-1))
    
    @property
    def slices(self,) -> List[slice]:
        """Return the slices associated to the bag."""
        # _slices has been constructed somewhere
        if hasattr(self, '_slices'):
            return self._slices
        # slices are empty if the bag does not exist
        elif np.prod(self.bag.shape) == 0:
            return []
        else:
            self._slices = self.construct_slices()
            return self._slices
    
    def construct_slices(self,) -> List[slice]:
        """Get all the slices that corresponds to the initial documents."""
        indices = self.bag[:,0].nonzero()[0]
        slices = [slice(start,stop) 
                  for start,stop in zip(indices[:-1],indices[1:])]
        last_stop = slices[-1].stop
        slices.append(slice(last_stop, self.bag.shape[0])) # the last one
        return slices 

    def reconstruct_document(self, n) -> List[str]:
        """Reconstruct the document number n in the intial catalog."""
        slice_ = self.slices[n]
        return self._reconstruct_document_from_slice(slice_)
    
    def reconstruct_catalog(self,) -> List[List[str]]:
        """Reconstruct all the catalog from the NoW matrix."""
        return [self._reconstruct_document_from_slice(slice_)
                for slice_ in self.slices]    

    def _reconstruct_document_from_slice(self, slice_: slice) -> List[str]:
        """Reconstruct a document when a slice is given. To be overwritten"""
        raise NotImplementedError
    
    @property
    def bow(self,) -> sp.csr_matrix:
        """BagOfWords object associated to the DirectedBag"""
        if not hasattr(self, "_bow"):
            self.construct_bow()
        return self._bow 
    
    def construct_bow(self,) -> None:
        """Transforms the DirectedBag into a BagOfWords, i.e. witout ordering of the tokens. 
        Returns None, but construct the `_bow` attribute, for the `bow` parameter that will be memoized."""
        t0 = dt.now()
        j_indices, indptr, values = list(), list(), list()
        indptr.append(0)
        generator = self._tqdm(self.slices)
        for slice_ in generator:
            docsparse = self.bag[slice_, :]
            count_idx = self._docsparse_to_countidx(docsparse)
            
            j_indices.extend(count_idx.keys())
            values.extend(count_idx.values())
            indptr.append(len(j_indices))

        j_indices = np.asarray(j_indices, dtype=int)
        indptr = np.asarray(indptr, dtype=int)
        values = np.asarray(values, dtype=int)

        bow = sp.csr_matrix((values,j_indices,indptr),
                            shape=(len(indptr)-1, len(self.vocabularray)),
                            dtype=int)
        bow.sort_indices()
        bow_dict = {k:v for k,v in self.__dict__.items()}
        bow_dict['bag'] = bow
        if self.verbose:
            delta = dt.now() - t0
            m = "{},".format(dt.now().strftime("%Y-%m-%d %H:%M:%S"))
            m += " {:.4} s".format(delta.total_seconds())
            m += " to convert {}".format(self.__class__.__name__)
            m += " into a BagOfWords"
            print(m)
        self._bow = BagOfWords(**bow_dict)
        return None 
    
    def _docsparse_to_countidx(self, docsparse) -> dict:
        """Transforms the docsparse (a sparse matrix representing a document) into a counting dictionnary in the form {token_id: number_of_tokens_in_the_document}. To be overwritten in children classes."""
        raise NotImplementedError
    
    @property
    def linids2docids(self,) -> np.array:
        """Convert line indices of the bag to slices indices of the directed bag.
        Returns an array of length bag.shape[0] containing the docids repeated for each line of bag corresponding to the docid."""
        if not hasattr(self, "_linids2docids"):
            self.construct_linids2docids()
        return self._linids2docids
    
    def construct_linids2docids(self,) -> None:
        """Constructs the `_linids2docids` attribute, for the `linids2docids` parameter that will be memoized.
        Returns None."""
        self._linids2docids = np.zeros(self.bag.shape[0], dtype=int)
        for i, sl in self._tqdm(enumerate(self.slices)):
            self._linids2docids[sl] = np.full(sl.stop-sl.start, i)
        return None
    
    @property
    def datids2docids(self,) -> np.array:
        """Convert data indices of the non-zero values into the docids. Allows to reattribute the docids to the non-zero values.
        Returns an array of the same length as bag.data or one of the bag.nonzero"""
        if not hasattr(self, "_datids2docids"):
            self.construct_datids2docids()
        return self._datids2docids
    
    def construct_datids2docids(self,) -> None:
        """Constructs the `_datids2docids` attribute, for the `datids2docids` parameter that will be memoized.
        Returns None."""
        bag = self.bag.tocsr()
        ptr = bag.indptr
        i, end = 0, len(bag.data)
        self._datids2docids = np.zeros(end, dtype=int)
        for i1, i2 in self._tqdm(zip(ptr[:-1], ptr[1:])):
            self._datids2docids[i1:i2] = np.full(i2-i1, self.linids2docids[i])
            i += 1
        self._datids2docids[i2:end] = np.full(end-i2, i)
        return None
    
    @property
    def boundatids(self,) -> np.array:
        """The array of line positions where the boundaries of the documents are located"""
        if not hasattr(self, "_boundatids"):
            self.construct_boundatids()
        return self._boundatids
    
    def construct_boundatids(self,) -> None:
        """Constructs the `_boundatids` attribute, for the `boundatids` parameters that will be memoized. Returns None"""
        # shift by one to get the true position
        boundaries = np.flatnonzero(np.diff(self.datids2docids))+1
        # the 0 position and the last position are missing
        self._boundatids = np.concatenate([np.array([0,]), 
                                           boundaries,
                                           np.array([len(self.data),])])
        return None
    
    @property
    def sortokids(self,) -> np.array:
        """The array of all token ids sorted from the first position of the first document to the last position of the last document. Recall that the boudaries of the documents are given by `boundatids`"""
        if not hasattr(self, "_sortokids"):
            self.construct_sortokids()
        return self._sortokids
    
    def construct_sortokids(self,) -> None:
        """Constructs the `_sortokids` attribute, for the `sortokids` parameters that will be memoized. Returns None"""
        raise NotImplementedError 
    
    def _line_numbers_to_docids(self, line_numbers):
        """Convert a collection of line number (in the bag representation) to a list of document id (in the doctionarray)"""
        slices_idx = self.linids2docids[line_numbers]
        slices_idx = tuple(set(slices_idx))
        return self.doctionarray[slices_idx]
        
    @property
    def adjacency_list(self,): 
        """A dictionary of dictionaries of the form {token_idx_1: {token_idx_2: count12, ...}} counting the number of times token[idx_1] is followed by token[idx_2], ... """
        if not hasattr(self, "_adjacency_list"):
            self._adjacency_list = self.construct_adjacency_list(tokens=False) 
        return self._adjacency_list
    
    def construct_adjacency_list(self, tokens=False):
        """
Take the BOW/NOW and returns a dictionary of dictionaries, indicating the token and its following tokens and their countings.

For instance, 
```python
{0: {1:5, 2:3, ...},
 1: {2:1, 4:6, ...},
}
```
signifies that the token 0 (as given in the `vocabularray`) is followed by token 1 five times, by token 2 three times, ... and the token 1 is followed by token 2 one time and by token 4 six times, ... 

If `tokens=True`, the tokens are returned, and no more their indices in the `vocabularray`. The above example then reads

```python
{'tok0': {'tok1':5, 'tok2':3, ...},
 'tok1': {'tok2':1, 'tok4':6, ...},
}
```
with the strings of the `vocabularray` replacing the `'tokx'` strings above.
        """
        count = self.construct_count_toknextok_tuples()
        # reconstruct a dict of dict {token_idx1: {nextoken_idx1: count1, 
        #                                          nextoken_idx2: count2, ...},
        #                             token_idx2: ... }
        adj_dict = defaultdict(dict)
        for (tokid, nextokid), c in self._tqdm(count.items()):
            adj_dict[tokid][nextokid] = c
        self._adjacency_list = dict(adj_dict)
        # map to tokens using the vocabularray
        if bool(tokens):
            adj_dict = {self.vocabularray[k]: {self.vocabularray[i]:j
                                               for i,j in v.items()}
                        for k,v in self._tqdm(adj_dict.items())}
        return dict(adj_dict)
    
    def construct_count_toknextok_tuples(self,): 
        """
Generates the list of `[token_idx, nextoken_idx]` and counts the associated tuples. It returns a dictionary of tuples that counts the tuples: 
```python
{(token_id1, nextoken_id1): count1, 
 (token_id2, nextoken_id2): count2, ...}
```
        """
        tuples = self.construct_toknextok(nb_nextoks=1)
        # count all tuples of the form (token_idx, nextoken_idx)
        count = dict(Counter(tuple(r) for r in self._tqdm(tuples)))
        return count
    
    def construct_toknextok(self, nb_nextoks: int=1) -> np.array:
        """Constructs the array of the form
        ```python
        [[tokid1, nextokid1, nextonextokid1, ...],
         [tokid2, nextokid2, nextonextokid2, ...],
         ...] 
        ```
        with number of columns = `nb_nextoks + 1`."""
        raise NotImplementedError

    @property
    def adjacency_matrix(self,) -> sp.csr_matrix:
        """A matrix of size VxV where (i,j) element corresponds to the number of times token[j] follows token[i]."""
        if not hasattr(self, "_adjacency_matrix"):
            self.construct_adjacency_matrix()
        return self._adjacency_matrix 
    
    def construct_adjacency_matrix(self,): 
        """
Take the NOW and returns an adjacency matrix `adj_mat` for oriented graph. The convention is that `adj_mat[i,j]=2` means that the token `i` is followed by the token `j` (the indices refer to the `vocabularray_` indices) two times. How many times the token `j` is followed by the token `i` is given by `adj_mat[j,i]`, which has no reason to be equal to `adj_mat[i,j]`

The outcome is a `scipy.sparse.csr_matrix` of size VxV, with V the `len(vocabularray)`.
        """
        adj_dict = self.adjacency_list
        s = self.shape[1]
        indptr, indices, data = [0,], list(), list()
        for i in self._tqdm(range(s)):
            count_dict = adj_dict.get(i, {})
            indptr.append(indptr[-1]+len(count_dict))
            indices.extend(count_dict.keys())
            data.extend(count_dict.values())
        adj_mat = sp.csr_matrix((data,indices,indptr), shape=(s,s), dtype=int)
        self._adjacency_matrix = adj_mat
        return None
    
    @property
    def pmi(self,): 
        """
        Calculate the pointwise mutual information (PMI, see definition on [Wikipedia/Mutual_information](https://en.wikipedia.org/wiki/Mutual_information#Applications_2)) [Wikipedia/Pointwise_mutual_information](https://en.wikipedia.org/wiki/Pointwise_mutual_information).
                                                                                        
Note that mutual information can be understood here as the _pointwise_ mutual information, since the mutual information is defined as the average over all elements of the pointwise mutual information.

Returns sparse matrix of size VxV, with V the length of the vocabularray.
        """
        if not hasattr(self, "_pmi"):
            self.construct_pmi()
        return self._pmi

    def construct_pmi(self, normalize=True) -> None:
        """Constructs the `_pmi` attribute, for the `pmi` parameter that will be memoized.
        Returns None."""
        vocount = self.bow.vocount
        nb_unigrams = np.sum(vocount)
        bicount = self.adjacency_matrix
        nb_bigrams = np.sum(bicount)
        norm = nb_unigrams*nb_unigrams/nb_bigrams
        idx, idy = bicount.nonzero()
        denom_data = vocount[idx]*vocount[idy]
        data = np.log(norm*bicount.data/denom_data)
        if normalize:
            data = data/np.log(nb_bigrams/bicount.data)
        self._pmi = sp.csr_matrix((data, bicount.indices, bicount.indptr), 
                                  shape=bicount.shape)
        return None
    
    @property
    def graph(self,):
        """Construct the directed graph associated to the Directed BagOfWords. Returns a networkx graph object."""
        if not hasattr(self, "_graph"):
            self._graph = nx.from_scipy_sparse_array(self.adjacency_matrix,
                                                     parallel_edges=False,
                                                     edge_attribute="weigth",
                                                     create_using=nx.DiGraph)
        return self._graph
    