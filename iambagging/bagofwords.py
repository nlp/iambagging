#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
`BagOfWords` class, which counts the number of times a word appears in a catalog, the number of distinct documents where a word appears, and the cooccurence matrix of all unique words in a catalog.

"""

from datetime import datetime as dt
from itertools import combinations
from collections import defaultdict

import numpy as np
import scipy.sparse as sp
from tqdm import tqdm
import scipy.spatial.distance as scidist
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.utils import shuffle as sklearn_shuffle

from .bagbase import BagBase

class BagOfWords(BagBase):
    """
`BagOfWords` class, which counts the number of times a word appears in a catalog, the number of distinct documents where a word appears, and the cooccurence matrix of all unique words in a catalog.

A catalog is defined as a sequence of sequences of strings, where each string is supposed to be a token. This class counts the number of tokens. It is based on the class `sklearn.feature_extraction.text.CountVectorizer` ([documentation](https://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.CountVectorizer.html)), which ouput some `scipy.sparse` sparse matrices ([documentation](https://docs.scipy.org/doc/scipy/reference/sparse.html)). Note that neither the `tokenizer` nor the `preprocessor` of this class is used, so the catalog can be passed in any way let free to the user.

 The size of each document is simply the size of the inner sequence, and this can be changed at will by the user. This preprocessing of the data, let to the user, consists in the main flexibility of the characteristics extracted by the `FeaturesCatalog` class. An example catalog is : 

 ```python
 catalog = [['string11', 'string12', 'string13', ... 'string1N1'],
            ['string21', 'string22', ..., 'string2N2'],
            ...,
            ]
 ```
The size of each inner sequence determines the window for latter analysis, like `cooccurence` or `cobow_equivalence` or `pointwise_mutual_information`.

    """
    
    @property
    def shape(self,):
        """Returns the sparse matrix shape."""
        return self.bag.shape
    
    def _generate_bag(self, catalog):
        """ Generate the `occurence` and `vocabulary` object either from a given catalog or from the `self.catalog_` in case no `catalog` is given. Does not give any `tokenizer` or `preprocessor` to `CountVectorizer`.
        """
        vocabulary = defaultdict()
        vocabulary.default_factory = vocabulary.__len__
        doctio, docid = list(), None
        
        j_indices, indptr, values = list(), list(), list()
        indptr.append(0)
        generator = self._tqdm(enumerate(catalog))
        for docid, doc in generator:
            if self.docextractor:
                docid, doc = self.docextractor(doc)
            token_counter = defaultdict(int)
            for token in doc:
                token_idx = vocabulary[token]
                token_counter[token_idx] += 1
            token_counter = dict(token_counter)
            j_indices.extend(token_counter.keys())
            values.extend(token_counter.values())
            indptr.append(len(j_indices))
            doctio.append(docid)

        # disable defaultdict behaviour
        vocabulary = dict(vocabulary)
        vocabularray = np.array(list(vocabulary.keys()), dtype=str)
        doctionarray = np.array(doctio)

        j_indices = np.asarray(j_indices, dtype=int)
        indptr = np.asarray(indptr, dtype=int)
        values = np.asarray(values, dtype=int)

        bag = sp.csr_matrix((values,j_indices,indptr),
                            shape=(len(indptr)-1, len(vocabulary)),
                            dtype=int)
        bag.sort_indices()
        return bag, vocabularray, doctionarray
    
    def make_boolean(self, sparse, withdraw_diag=False):
        """Transform a sparse matrix into the same matrix, where all entries are replaced with one (for True) and zeros are let the same. If `withdraw_diag` (a Boolean) is True, then the diagonal is withdrawn. For later uses."""
        sparse = sp.csr_matrix(sparse)
        data = np.ones(shape=sparse.data.shape)
        indices, indptr = sparse.indices, sparse.indptr
        boolean = sp.csr_matrix((data,indices,indptr),
                                shape=sparse.shape, dtype=int)
        if bool(withdraw_diag):
            data = boolean.diagonal()
            indices = np.arange(np.min(sparse.shape))
            indptr = np.arange(np.min(sparse.shape)+1)
            diagonal = sp.csr_matrix((data,indices,indptr),
                                     shape=sparse.shape, dtype=int)
            boolean = boolean - diagonal
        return boolean
    
    def count(self, axis=0, unique=False):
        """
Counts the number of times a unique tokens happens in a catalog, given as parameter. 
        
If `axis=0` (by default), returns a `numpy.array` of shape (1,W) (`vocabularray_` size) with number of occuring token in the entire catalog.

If `axis=1`, returns a `numpy.array` of shape (1,D) (number of documents) with number of tokens per document.

If `unique=True`, replaces all entries in the BOW by 1 before calculating the sum. That is : 
    - if `axis=0` (by default), returns the number of tokens observed, while counting one token per document at maximum
    - if `axis=1`, returns the number of unique tokens per document.
        """
        self._check_init()
        bag = sp.csr_matrix(self.bag)
        if bool(unique):
            bag = self.make_boolean(bag, withdraw_diag=False)            
        if int(axis) == 0:
            bag = sp.csc_matrix(bag)
        elif int(axis) == 1:
            bag = sp.csr_matrix(bag)
        else:
            raise ValueError("axis must be either 0 or 1")
        data, indptr = bag.data, bag.indptr
        count_list = [np.sum(data[i:j]) 
                      for i,j in self._tqdm(zip(indptr[:-1], indptr[1:]))] 
        print("charge in array")
        count_array = np.array(count_list, dtype=int)
        return count_array
    
    @property 
    def vocount(self,):
        """Total frequency of each token in the vocabularray. Returns a numpy array of length V, the size of the vocabularray."""
        if not hasattr(self, "_vocount"):
            self.construct_vocount()
        return self._vocount
    
    def construct_vocount(self,): 
        """Construct the total frequency of each token in the vocabularray. Returns a numpy array of length V, the size of the vocabularray."""
        count = self.count(axis=0, unique=False) 
        self._vocount = count.reshape(len(self.vocabularray))
        return None 

    @property 
    def docount(self,):
        """Total frequency of each token in the vocabularray. Returns a numpy array of length V, the size of the vocabularray."""
        if not hasattr(self, "_docount"):
            self.construct_docount()
        return self._docount    
    
    def construct_docount(self,):
        """Number of tokens per document in the doctionarray. Returns a numpy array of length D, the size of the (non-empty document) catalog."""
        count = self.count(axis=1, unique=True)
        self._docount = count.reshape(len(self.doctionarray))
        return None 
    
    def entropy(self,axis=0,normalize=True):
        """
Calculate the [entropy](https://en.wikipedia.org/wiki/Entropy_(information_theory)) according to its definition in information theory, along a given `axis` of the BOW. This is just the sum over its axis elements times their natural logarithms. If `normalize=True` (value by default), then the logarithm in base N (with N the total number of tokens in the BOW) is used, and all elements of the BOW are divided by N as well. Then it becomes possible to compare the different entropies disregarding the size of the BOW.
        """
        self._check_init()
        X = sp.csr_matrix(self.bag)
        Lx = sp.csr_matrix((-np.log(X.data),X.indices,X.indptr),
                           shape=X.shape,dtype=float)
        H = X.multiply(Lx) # matrix of local entropies
        E = np.array(np.sum(H,axis=axis))
        if bool(normalize):
            sum_X = np.sum(X)
            sum_axis = np.array(np.sum(X,axis=axis))/sum_X
            N = sum_X*np.log(sum_X)
            E = sum_axis-E/N
        return E
    
    def cooccur(self, axis=0):
        """
Calculate the cooccurence matrix along the axis of the BOW matrix. Hereafter, a 'unique token' is a token appearing in the BOW, with its counting number equal to 1, that is, before any calculation, the entries of the BOW are resetted to 1.

If `axis=0`, returns the `cooccurence` matrix of size W x W, where W is the number of unique tokens in the catalog, with non-zeros entries counting the number of times two tokens appear in the same document.

If `axis=1`, returns the `cooccurence` matrix of size D x D, where D is the number of documents in the catalog, with non-zeros entries counting the number of times two documents have the same tokens in commom. This matrix can be seen as the adjacency matrix of the line graph of the cooccurence with `axis=0`, and vice-versa.

Note that the diagonal elements are not reseted to zero. That is, the diagonal elements `cooccurence[i,i]` counts 
 - the number of unique tokens in the document in case `axis=1`, i.e. the size of the document i in term of unique token
 - the number of documents a unique token i appeared in, in case `axis=0`
These matrices can be seen as the Laplacian matrices of the graph and line graph, respectively, of the cooccurence matrices. That is, the degrees are the diagonal components, and the adjacency matrix corresponds to the non-diagonal elements.
        """
        t0 = dt.now()
        self._check_init()
        ones_matrix = self.make_boolean(self.bag)
        if axis==1:
            ones_matrix = ones_matrix.T
        cooccurence = ones_matrix.T.dot(ones_matrix)
        if self.verbose:
            mess = "{:.4}".format((dt.now()-t0).total_seconds())
            mess += " s to calculate cooccurence matrix"
            print(mess)    
        return cooccurence
    
    def tfidf(self,**kwargs):
        """
Calculate the TF-IDF according to [sklearn](https://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.TfidfTransformer.html). Take all arguments of the sklearn class. 

Returns a new `BagOfWords` instance with the TF-IDF matrix as `bow`, and the previous `vocabularray` attributes.
        """
        self._check_init()
        tfidf = TfidfTransformer(**kwargs)
        tfidf = tfidf.fit_transform(self.bag)
        features = BagOfWords(self)
        features.bag = tfidf
        return features

    def cosine_similarity(self, axis=0):
        """
Calculate the cosine similarity along the axis of the BOW matrix. Returns a symmetric sparse matrix whose entries correspond to non-null similarity along the given axis.

If the BOW matrix is a DxW sparse matrix, calculating the `cosine_similarity` along the `axis=0` returns a WxW matrix, and a DxD symmetric matrix in case `axis=1` is chosen.
        """
        t0 = dt.now()
        sparse = self.bag
        if axis==1:
            sparse = sparse.T
        symmetric = sparse.T.dot(sparse)
        diagonal = symmetric.diagonal().reshape(1,symmetric.shape[axis])
        diagm1 = 1/np.sqrt(diagonal)
        symmetric = symmetric.multiply(diagm1)
        symmetric = symmetric.multiply(diagm1.T)
        if self.verbose:
            mess = "{:.4}".format((dt.now()-t0).total_seconds())
            mess += " s to calculate cosine_similarity matrix"
            print(mess)    
        return symmetric.tocsr()

    def jaccard_similarity(self, axis=0):
        """
Calculate the Jaccard similarity along the axis of the BOW matrix. Returns a symmetric sparse matrix whose entries correspond to non-null similarity along the given axis. This Jaccard similarity is defined as on [Wikipedia/Jaccard_index](https://en.wikipedia.org/wiki/Jaccard_index) and [scipy.spatial.distance.jaccard](https://docs.scipy.org/doc/scipy/reference/generated/scipy.spatial.distance.jaccard.html), except it is faster since it use broadcasting of the entire sparse matrix. 

Note the Jaccard similarity is defined when disregarding counting of the tokens in the BOW, i.e. all entries of the BOW are replaced with 1 before proceeding to calculation. In that sense, the Jaccard similarity is the [similarity of binary attributes](https://en.wikipedia.org/wiki/Jaccard_index#Similarity_of_asymmetric_binary_attributes). In particular, it means the Jaccard similarity calculated here is not exactly the [Jaccard score from sklearn.metrics.jaccard_score](https://scikit-learn.org/stable/modules/generated/sklearn.metrics.jaccard_score.html) as it would be calculated from the BOW, since the sklearn implementation uses [multi label criterion and average](https://scikit-learn.org/stable/modules/model_evaluation.html#average).

If the BOW matrix is a NxM sparse matrix, calculating the `jaccard_similarity` along the `axis=0` returns a MxM matrix, and a NxN symmetric matrix in case `axis=1` is chosen.
        """
        t0 = dt.now()
        sparse = self.make_boolean(self.bag)
        if axis==1:
            sparse = sparse.T
        symmetric = sparse.T.dot(sparse)
        diagonal = symmetric.diagonal()
        indices = symmetric.nonzero()
        datas = np.array(symmetric[indices[0],indices[1]]).flatten()
        denom = diagonal[indices[0]]+diagonal[indices[1]]-datas
        symmetric = sp.csr_matrix((datas/denom,indices),
                                  shape=symmetric.shape,dtype=float)
        if self.verbose:
            mess = "{:.4}".format((dt.now()-t0).total_seconds())
            mess += " s to calculate jaccard_similarity matrix"
            print(mess)    
        return symmetric.tocsr()

    def graph_equivalence(self, adj_mat, make_boolean=True):
        """
Calculate the number of disconnected graphs from an [adjacency matrix](https://en.wikipedia.org/wiki/Adjacency_matrix) `adj_mat`, supposed to represent an undirected graph. Returns a list of sets of indices, each set representing one class, and the indices being the indices of the matrix which belong to the class.

If `make_boolean` is True, the adjacency matrix is renderered boolean, i.e. a [distance matrix](https://en.wikipedia.org/wiki/Distance_matrix) is then transformed into a correct [adjacency matrix](https://en.wikipedia.org/wiki/Adjacency_matrix). In that case the diagonal elements are removed. Default is False.
        """
        t0 = dt.now()
        if bool(make_boolean):
            adj_mat = self.make_boolean(adj_mat,withdraw_diag=True)
        n, labels = sp.csgraph.connected_components(adj_mat,
                                                    directed=False,
                                                    return_labels=True)
        indices = np.arange(adj_mat.shape[0])
        equivalences = [set(indices[labels==i]) 
                        for i in range(np.max(labels)+1)]
        if self.verbose:
            mess = "{:.4}".format((dt.now()-t0).total_seconds())
            mess += " s to calculate equivalence classes"
            print(mess)
        return equivalences

    def graph_equivalence_count(self, adj_mat, make_boolean=True):
        """
Calculate the number of disconnected graphs from an adjacency matrix. Returns the number of equivalence classes.

If one only needs the number of equivalence class, this method is faster than extracting `len(equivalences)` with `equivalence` the outcome of `graph_equivalence` method.
        """
        t0 = dt.now()
        if bool(make_boolean):
            adj_mat = self.make_boolean(adj_mat,withdraw_diag=True)
        n = sp.csgraph.connected_components(adj_mat,
                                            directed=False,
                                            return_labels=False)
        if self.verbose:
            mess = "{:.4}".format((dt.now()-t0).total_seconds())
            mess += " s to calculate equivalence classes number"
            print(mess)
        return n
    
    def LMI(self,axis=0,normalize=True):
        """Alias for the `loose_mutual_information method."""
        return self.loose_mutual_information(axis=axis,normalize=normalize)
    
    def loose_mutual_information(self, axis=0, normalize=True):
        """
Something like the method `pointwise_mutual_information` (MI), except it is done directly using the `cooccur` attribute of the object. That is, there is no notion of real bigrams ; entries are simply unordered set of words comprised in the size of a document in the catalog. This method gives a looze approximation of the (pointwise) MI, without the necessity to parse twice a catalog.

Returns a list of (increasing) order pseudo MI in the form

```python
[({'string1','string2'}, score1), ({'string1','string3'}, score2), ...]
```
with the `score` given by `score = log(alpha*cooccur[i,j]/(occur[i]*occur[j]))` with the coefficient `alpha = nb_occur**2/nb_cooccur`, with 
 - `coccur[i,j]` the cooccurence between string `i` and `j`, 
 - `occur[i]` the occurence of the string `i`
 - `nb_occur = sum(occur[i] over i)` is the total occurence of strings
 - `nb_cooccur = sum(cooccur[i,j] over i,j)` is the total number of cooccurences
        """
        t0 = dt.now()
        self._check_init()
        cooccurence = self.cooccur(axis=axis)
        # withdraw the diagonal elements
        data = cooccurence.diagonal()
        indices = np.arange(np.min(cooccurence.shape))
        indptr = np.arange(np.min(cooccurence.shape)+1)
        diagonal = sp.csr_matrix((data,indices,indptr),
                                 shape=cooccurence.shape,dtype=int)
        cooccurence = cooccurence - diagonal
        unigrams_sum = np.sum(self.bag)
        bigrams_sum = np.sum(cooccurence)
        indices = cooccurence.nonzero()
        shape = cooccurence.shape
        lxy = np.log(np.array(cooccurence[indices[0],indices[1]]))
        lxy = lxy.reshape(lxy.shape[-1],)
        count_occur = np.array(np.sum(self.bag,axis=axis))
        count_occur = count_occur.reshape(1,shape[axis])
        lxly = np.log(count_occur[0,indices[0]]*count_occur[0,indices[1]])
        pmi = 2*np.log(unigrams_sum)-np.log(bigrams_sum)+lxy-lxly
        if normalize:
            pmi = pmi/(np.log(bigrams_sum)-lxy)
        PMI = sp.csr_matrix((pmi,indices),shape=shape)
        if self.verbose:
            mess = "{:.4}".format((dt.now()-t0).total_seconds())
            mess += " s to calculate loose mutual information"
            print(mess)
        return PMI

    def new_tokens_per_document(self, shuffle=False, fit=True):
        """
Evaluate the number of new tokens discovered per opened document. Returns `new_words` a `numpy.array` of integers of size D (the number of documents in the catalog), representing the number of new documents found from the previous position to the actual one, that is, if `new_words[d] = 12`, it means that there is `12` more tokens in document `d` than there were in after document `d-1` was opened.

If `fit=True` (default to `True`), fit the standardized cumulative sum of `new_words` to a Beta distribution, with parameters `alpha` and `beta`. Otherwise both `alpha` and `beta` are `None`. This fit is based on the maximum likelihood estimates of the probability distribution of `new_words`, seen as the probability distribution function of a Beta probability law. `alpha` and `beta` are defined according to the [Wikipedia page on Beta distribution](https://en.wikipedia.org/wiki/Beta_distribution).

As the order of opening documents is a mater of probability, one can shuffle the catalog before extracting `new_words`, with `shuffle=True` (default is `False`, in which case the rows of the `bow_` matrix are parsed in their actual order).

Returns the tuple `new_words, alpha, beta`
        """
        t0 = dt.now()
        self._check_init()
        occurence = self.bag
        if bool(shuffle):
            occurence = sklearn_shuffle(occurence)
        occurence = occurence.tocsc()
        indptr = occurence.indptr
        indices = occurence.indices
        first_time = np.array([indices[i] for i in indptr[:-1]], dtype=int)
        new_words = np.array([np.sum(first_time==i) 
                              for i in self._tqdm(range(occurence.shape[0]))],
                             dtype=int)
        if bool(fit):
            pdf = new_words[1:] / np.sum(new_words)
            x = np.linspace(0,1,new_words.shape[0])
            mu = np.sum(x[1:]*pdf)
            var = np.sum((x[1:]-mu)**2*pdf)
            # calculate the maximum likelihood estimates of alpha and beta
            phi = mu*(1-mu)/var-1 # _phi=alpha+beta
            alpha = mu*phi
            beta = (1-mu)*phi
        else:
            alpha, beta = None, None
        if self.verbose:
            mess = "{:.4}".format((dt.now()-t0).total_seconds())
            mess += " s to calculate new tokens per document"
            print(mess)
        return new_words, alpha, beta
    
    def symmetric2list(self,sparse,axis=1):
        """
Once a symmetric matrix is calculated somewhere else in this class, this method returns its non-zeros values in the form of an ordered list of tuples in the form: 
```python
[({tok11,tok12},dist1),
 ({tok21,tok22},dist2),
 ]
```
with `tokx` some words in the `vocabularray_` (if `axis=1`, which is the value by default), or `tokx` some indices (if `axis=0`). Then the list is ordered in increasing values of `distx`, which is a float.

In fact this method just present the ordered version of the symmetric matrix in the form of an ordered list.

**Warnings :** 
 - the returned list will never contain the diagonal elements of the sparse matrix, even if they exist in the original matrix.
 - there is anything to prevent from giving a non-symmetric matrix.
        """
        indices = [{i,j} 
                   for i,j in combinations(set(sparse.nonzero()[0]),2) 
                   if i!=j]       
        if axis==1:
            correlations = [(set(self.vocabularray[(i,j),]),sparse[i,j])
                            for i,j in indices]
        elif axis==0:
            correlations = [(set([i,j]),sparse[i,j]) for i,j in indices]
        else:
            mess = "axis should be either 0 or 1"
            raise AttributeError(mess)
        bigrams = np.array([c[0] for c in correlations])
        correls = np.array([c[1] for c in correlations])
        indices = np.argsort(correls)
        ordered = [(b,s) for b,s in zip(bigrams[indices],correls[indices])]
        return ordered
    
    def _remove_empty(self, bag, vocab, doctio):
        check_empty = np.diff(bag.indptr) != 0
        new_bag = bag[check_empty,:]
        idx, idy = new_bag.nonzero()
        new_vocab = vocab[tuple(set(idx)),]
        new_doctio = doctio[tuple(set(idy)),]
        return new_bag, new_vocab, new_doctio
    
    def filter_cols(self, conserved_indices, remove_empty=True):
        """Withdraw the columns of the BOW whose indices are not in the `conserved_indices`, and remove empty rows if `remove_empty=True`, to be integrated in later methods. Returns new `bag`, `vocabularray` and `doctionarray` attributes."""
        new_bag = self.bag[:,tuple(conserved_indices)]
        new_vocab = self.vocabularray[tuple(conserved_indices),]
        new_doctio = self.doctionarray
        if bool(remove_empty): # remove empty rows and filter doctionarray
            new_bag = new_bag.tocsr()
            check_empty = np.diff(new_bag.indptr) != 0
            new_bag = new_bag[check_empty, :]
            new_doctio = new_doctio[check_empty,]
        return new_bag, new_vocab, new_doctio
    
    def filter_rows(self, conserved_indices, remove_empty=True):
        """Withdraw the rows of the BOW whose indices are not in the `conserved_indices`, and remove empty columns if `remove_empty=True`, to be integrated in later methods. Returns new `bag`, `vocabularray` and `doctionarray` attributes."""
        new_bag = self.bag[tuple(conserved_indices),:]
        new_vocab = self.vocabularray
        new_doctio = self.doctionarray[tuple(conserved_indices),]
        if bool(remove_empty): # remove emtpy columns and filter vocabularray
            new_bag = new_bag.tocsc()
            check_empty = np.diff(new_bag.indptr) != 0
            new_bag = new_bag[:, check_empty]
            new_vocab = new_vocab[check_empty,]
        return new_bag, new_vocab, new_doctio

    def filter_documents_equivalent(self,):
        """
Withdraw all the document being equivalent to an other one. Two documents are considered equivalent when they present the same tokens, disregarding the number of occurence of each token in a document.
    
Returns new `bag`, `vocabularray` and `doctionarray` attributes, that can be given to a new `BagOfWords` instance, and containing only the kept tokens from the original BOW. Do not change the actual `BagOfWords` instance.

Consider `reconstruct_catalog` as well, which does something quite similar.
        """
        indptr = self.bag.indptr
        indices = self.bag.indices
        new_catalog = (set(indices[i:j]) for i,j in zip(indptr[:-1],indptr[1:]))
        clean_catalog, clean_indptr, clean_doctio = list(), [0,], list()
        cursor = 0
        for i,s in enumerate(new_catalog):
            if s not in clean_catalog:
                clean_doctio.append(self.doctionarray[i])
                clean_catalog.extend(list(s))
                cursor += len(s)
                clean_indptr.append(cursor)
        indices = np.array(clean_catalog, dtype=int)
        indptr = np.array(clean_indptr, dtype=int)
        data = np.ones(shape=(len(clean_catalog),), dtype=int)
        new_bag = sp.csr_matrix((data,indices,indptr))
        new_vocab = self.vocabularray
        new_doctio = clean_doctio
        if self.verbose:
            n = i+1-len(clean_catalog)
            print("{:,} documents withdrawn".format(n))
        return new_bag, new_vocab, new_doctio
    
    def reconstruct_catalog(self, withdraw_duplicates=True):
        """
Reconstruct a factice catalog from the BOW. The order of the tokens in the initial documents is not conserved. Usefull once a BOW has been filtered.      

Returns a new catalog (list of lists of tokens) which can be fed to a `BagOfWords().fit(new catalog)` to give back the actual one. The order of the token from the initial catalog is not the same.
        """
        catalog = [list(self.vocabularray[line.nonzero()[1],])
                   for line in self.bag.tocsr()]
        if bool(withdraw_duplicates):
            new_catalog = [set(doc) for doc in catalog]
            clean_new_catalog = list()
            for s in new_catalog:
                if s not in clean_new_catalog:
                    clean_new_catalog.append(s)
            catalog = [list(s) for s in clean_new_catalog]
        return catalog
            