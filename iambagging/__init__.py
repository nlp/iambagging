#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from pkg_resources import get_distribution
__version__ = get_distribution("iambagging").version

from .bagbase import BagBase
from .bagofwords import BagOfWords
from .arrowofwords import ArrowOfWords
from .nextofwords import NextOfWords