# -*- coding: utf-8 -*-

# IAM SkLearn Features

## Description

`iam-bagofwords` package aims at exploring a Bag Of Words using graph theory tools. Among its possibilities are : 
 - generation of a BagOgWords (BOW, a sparse matrix of size DxV shape, with D the size of the catalog, and V the size of the vocabulary)
 - generation of ArrowOfWords (AOW, a sparse matrix of artificial size (D')x(V+1) shape, recording the position of the tokens in the documents)
 - generation of NextOfWords (NOW, a sparse matrix of artificial size (D')x(V+1) shape, recording the position of the next token in the document)
 - counting of tokens, documents, 
 - transformation of the sparse matrix to adjacency matrix
 - equivalence classes construction of documents and/or tokens based on the adjacency matrix representing the BOW
 - pointwise mutual information with (AOW) and without (BOW) ordering of tokens
 - filtering of the BOW and AOW with respect to tokens and/or documents
 
## Dependency of the package

`iam-bagofwords` requires the following packages
 - `numpy` and`scpipy` for array manipulation
 - `sklearn` for shuffling and TF-IDF
 - `networkx` for graph manipulation and vizualization
 - `tqdm`  for verbosity

in addition to the standard Python library : `itertools`, `json`, `time`.

## Installation

To install this package, please clone it : 

```bash
git clone https://eds.chu-bordeaux.fr/gitlab/taliam/bagofwords
```

and then install the repository on top of your local Python library, using e.g. PythonPackageInstaler (pip)

```bash
python3 -m pip install dist/bagofwords
```

(eventually change for the correct version name). Then call the different packages as (adapt eventually the names of the classes you want to use)

```python
from iambagging import BagOfWords, NextOfWords
```

in your favorite Python console, and follow subsequent documentations.

## About us
 
Package developped for Natural Language Processing at IAM : Unité d'Informatique et d'Archivistique Médicale, Service d'Informatique Médicale, Pôle de Santé Publique, Centre Hospitalo-Universitaire (CHU) de Bordeaux, France.

Last version : Dec. 14, 2020