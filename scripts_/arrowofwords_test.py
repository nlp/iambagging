#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test des fonctionnalité de graphes de `BagOfWords`

"""

from time import time
import numpy as np
import scipy.sparse as sp
import matplotlib.pyplot as plt

from bagofwords import ArrowOfWords
import generate_random_catalog as catalog

cat = catalog.test_catalog

aow = ArrowOfWords()

bow = aow.fit_transform(cat)

aow.vocabulary_
bow.todense()
cat

aow.fit_transform(cat)
aow.bow_.todense()

slices = aow._get_doc_slices()
aow.bow_[slices[0],1:].data
aow.bow_[slices[0],1:].indices
aow.bow_[slices[0],1:].indptr
aow.bow_[slices[0],1:].nonzero()

bow = aow.bow_.tocsc()
for i in range(1,bow.shape[1]):
    col = bow[:,i].data
    print(col)

nexts = aow.nexts()
nexts_token = {aow.vocabularray_[k]: {aow.vocabularray_[i] for i in v} 
               for k,v in nexts.items()}
aow.vocabularray_

vocab, now = aow._generate_now(cat)
now.todense()
cat[0]
vocab
