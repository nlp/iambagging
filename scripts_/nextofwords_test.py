#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test des fonctionnalité de graphes de `BagOfWords`

"""

from time import time
import numpy as np
import scipy.sparse as sp
import matplotlib.pyplot as plt

from bagofwords import NextOfWords
import generate_random_catalog as catalog

cat = catalog.test_catalog

now = NextOfWords()

now.fit_transform(cat)
now.bow_.todense()

slices = now._get_doc_slices()

now.shape
len(cat)

nexts = now.adjacency_list(tokens=True)
nexts = now.adjacency_list()
now.vocabularray_

import networkx as nx
import matplotlib.pyplot as plt

G = nx.DiGraph()
G.add_nodes_from(range(now.shape[1]))
G.number_of_nodes()
G.number_of_edges()

adj_list = now.adjacency_list()
weighted_edges = list()
for i in range(len(adj_list)):
    adj_dict = adj_list[i]
    weighted_edges.extend((i,k,v) for k,v in adj_dict.items())
G.add_weighted_edges_from(weighted_edges)

# in one shot, integer graph
G = nx.DiGraph()
G.add_nodes_from(range(now.shape[1]))
adj_list = now.adjacency_list()
gen = ((i,k,v) for i in adj_list.keys()
       for k,v in adj_list[i].items())
G.add_weighted_edges_from(gen)

# in one shot, string graph
G = nx.DiGraph()
G.add_nodes_from(now.vocabularray_)
adj_list = now.adjacency_list(tokens=True)
gen = ((i,k,v) for i in adj_list.keys() 
       for k,v in adj_list[i].items())
G.add_weighted_edges_from(gen)

nx.draw(G)
nx.draw_networkx_nodes(G)
nx.draw_networkx_edges(G)

nx.readwrite.adjlist.write_adjlist(G,'networkx_test_adjlist')

list(G.predecessors('word1'))
list(G.successors('word1'))
G.degree(1)
