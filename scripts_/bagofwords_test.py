#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test des fonctionnalité de graphes de `BagOfWords`

"""

from time import time
import numpy as np
import scipy.sparse as sp
import matplotlib.pyplot as plt

from bagofwords import BagOfWords as BOW
import generate_random_catalog as catalog

bow = BOW(verbose=False).fit(catalog.test_catalog)

# check catalog size
words_set = {w for c in catalog for w in c}
unique_words = len(words_set)
unique_doc = len(catalog)
bow.vocabularray_.shape == (1,unique_words)
bow.bow_.shape == (unique_doc,unique_words)

count = bow.count(axis=0, unique=False)
count_dict2 = {bow.vocabularray_[0,i]:count[0,i] for i in range(bow.bow_.shape[1])}

count0 = bow.count(axis=0, unique=True)
c0 = bow.cooccur(axis=0)
diag = c0.diagonal()
mask = count0==diag
all(mask.flatten())

count1 = bow.count(axis=1, unique=True)
c1 = bow.cooccur(axis=1)
diag = c1.diagonal()
mask = count1.flatten()==diag.flatten()
all(mask.flatten())


c1 = bow.cooccur(axis=1)

cat = catalog.random_catalog(size_vocab=1000,size_documents=[2,10],size_catalog=250)
bow = BOW(verbose=False).fit(cat)
c0 = bow.cooccur(axis=0)
bow.graph_equivalence_count(c0, make_boolean=True)
equiv = bow.graph_equivalence(c0, make_boolean=True)

vocabulary = ['w'+str(i) for i in range(1000)]
bow = BOW(verbose=False,vocabularray=vocabulary,bow=np.arange(1000))
bow.fit(cat)
c0 = bow.cooccur(axis=0)
bow.graph_equivalence_count(c0, make_boolean=True)

l_equiv = [len(e) for e in equiv]

bow.entropy(axis=1)
E = bow.entropy(axis=0)
E[0,np.argsort(E)][0,-50:]
bow.vocabularray_[np.argsort(E)[-50:],]
bow.entropy(axis=1,normalize=False)
bow.entropy(axis=0,normalize=False)

bow.symmetric2list(bow.cosine_similarity(axis=1),axis=1)


new_words, alpha, beta = bow.new_tokens_per_document()
sum(new_words)

bow.shape(12)

word_in_vocabularray = [word in bow.vocabularray_ for word in words_set]
all(word_in_vocabularray)

M = np.concatenate([np.ones(5,dtype=int),np.zeros(20,dtype=int)])
M = np.random.permutation(M).reshape(5,5)
M = (M+M.T)
vocab = np.arange(5)

feat = FeaturesBagOfWords(vocabularray=vocab,occurence=M)

boolean = feat._make_boolean(M,withdraw_diag=True)
boolean = np.array(boolean.todense(),dtype=int)

feat.graph_equivalence(boolean,make_boolean=True)

boolean = sp.csr_matrix(boolean)
inds = boolean.nonzero()
vertices = [(i,j) for i,j in zip(inds[0], inds[1])]

from igraph import Graph

G = Graph(vertices)

G.vertex_disjoint_paths()
